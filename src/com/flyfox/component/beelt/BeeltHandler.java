package com.flyfox.component.beelt;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.ext.jfinal.BeetlRenderFactory;

import com.jfinal.handler.Handler;

/**
 * TODO Beelt拦截器
 * 未完成
 * 
 * @author flyfox 2014-2-11
 */
public class BeeltHandler extends Handler {

	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean isHandled[]) {
		if (target.indexOf(".bee") > 0) {
			try {
				String path = request.getContextPath();
				String basePath = request.getScheme() + "://" + request.getServerName() //
						+ ":" + request.getServerPort() + path + "/";
				GroupTemplate gt = BeetlRenderFactory.groupTemplate;
				Template t = gt.getTemplate(target);
				t.binding("BASE_PATH", basePath);
				OutputStream out = response.getOutputStream();
				t.renderTo(out);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			nextHandler.handle(target, request, response, isHandled);
		}
	}

}
